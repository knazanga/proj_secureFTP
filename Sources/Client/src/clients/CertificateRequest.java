package clients;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;

public class CertificateRequest extends Connection {

    public static final String ASYMETRIC_ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    public static final String PROVIDER            = "BC";
    public static final String CLIENT_NAME         = "Bob";
    X509Certificate            mycert;
    X509Certificate            caCert;
    private String             keypass;
    private String             keystorefile;
    PrivateKey                 key;

    public CertificateRequest( String addr, Integer port, String file, String pass ) {
        super( addr, port );
        this.keystorefile = file;
        this.keypass = pass;
    }

    @Override
    public void run() throws Exception {
        Security.addProvider( new BouncyCastleProvider() );
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance( ASYMETRIC_ALGORITHM );
        KeyPair kp = keyPairGenerator.generateKeyPair();
        this.key = kp.getPrivate();
        PKCS10CertificationRequest request = generateCertificateRequest( CLIENT_NAME, kp );
        byte[] bytes = request.getEncoded();
        output.write( bytes );
        byte[] rep = this.read();
        X509Certificate certCA = (X509Certificate) CertificateFactory.getInstance( "X.509" )
                .generateCertificate( new ByteArrayInputStream( rep ) );
        output.write( "OK".getBytes() );
        rep = this.read();
        X509Certificate cert = (X509Certificate) CertificateFactory.getInstance( "X.509" )
                .generateCertificate( new ByteArrayInputStream( rep ) );
        if ( cert == null || certCA == null ) {
            this.errorMsg = new String( rep );
            throw new Exception();
        } else {
            this.mycert = cert;
            this.caCert = certCA;
        }
        this.close();
    }

    public static PKCS10CertificationRequest generateCertificateRequest( String name, KeyPair kp ) throws Exception {
        Security.addProvider( new BouncyCastleProvider() );
        KeyPair keys = kp;
        X500Name subjectName = new X500Name( "cn=" + name );
        SubjectPublicKeyInfo keyInfo = SubjectPublicKeyInfo.getInstance( keys.getPublic().getEncoded() );
        PKCS10CertificationRequestBuilder csrgen = new PKCS10CertificationRequestBuilder( subjectName, keyInfo );
        ContentSigner contentSigner = new JcaContentSignerBuilder( SIGNATURE_ALGORITHM ).setProvider( PROVIDER )
                .build( keys.getPrivate() );
        return csrgen.build( contentSigner );
    }

    public boolean storeCertAndKey( KeyStore ks, String alcert, String alkey, String alpass, String alCaCert ) {
        try {
            ks.setCertificateEntry( alcert, this.mycert );
            ks.setCertificateEntry( alCaCert, caCert );
            ks.setKeyEntry( alkey, this.key, alpass.toCharArray(), new Certificate[] { this.mycert } );
            ks.store( new FileOutputStream( keystorefile ), keypass.toCharArray() );
            return true;
        } catch ( Exception e ) {
            return false;
        }
    }
}
