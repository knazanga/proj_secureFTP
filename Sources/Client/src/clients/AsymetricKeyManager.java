package clients;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class AsymetricKeyManager {
    public static final String CIPHER_ALGORITM = "RSA";
    public static final String SIGN_ALGORITHM  = "SHA1withRSA";

    public static byte[] cipher( X509Certificate cert, byte[] data ) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        try {
            Cipher cipher = Cipher.getInstance( CIPHER_ALGORITM );
            cipher.init( Cipher.ENCRYPT_MODE, cert.getPublicKey() );
            return cipher.doFinal( data );
        } catch ( Exception e ) {
            return null;
        }
    }

    public static byte[] decipher( PrivateKey key, byte[] data ) {
        try {
            Cipher cipher = Cipher.getInstance( CIPHER_ALGORITM );
            cipher.init( Cipher.DECRYPT_MODE, key );
            return cipher.doFinal( data );
        } catch ( Exception e ) {
            return null;
        }
    }

    public static byte[] sign( PrivateKey key, byte[] data ) {
        try {
            Signature sig = Signature.getInstance( SIGN_ALGORITHM );
            sig.initSign( key );
            sig.update( data );
            return sig.sign();
        } catch ( Exception e ) {
            return null;
        }
    }

    public static boolean verifySig( X509Certificate cert, byte[] dataoriginal, byte[] datasigned ) {
        try {
            Signature sig = Signature.getInstance( SIGN_ALGORITHM );
            sig.initVerify( cert.getPublicKey() );
            sig.update( dataoriginal );
            return sig.verify( datasigned );
        } catch ( Exception e ) {
            return false;
        }
    }
}
