package clients;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class Authentification extends Connection {
    PublicKey       caPublicKey;
    boolean         isServer;
    X509Certificate myCert;
    PrivateKey      myKey;
    byte[]          session;
    X509Certificate certB;

    public Authentification( String ip, Integer port, Socket s, X509Certificate cA,
            PrivateKey kA, PublicKey pk ) {
        super( ip, port );
        this.s = s;
        this.myCert = cA;
        this.myKey = kA;
        this.caPublicKey = pk;
    }

    public void bind() {
        try {
            this.output = new DataOutputStream( this.s.getOutputStream() );
            this.input = new DataInputStream( new DataInputStream( this.s.getInputStream() ) );
        } catch ( IOException e ) {
            this.finishedOK = false;
        }
    }

    @Override
    public void run() {
        try {
            this.output.write( this.myCert.getEncoded() );
            byte[] rep = this.read();
            this.certB = (X509Certificate) CertificateFactory.getInstance( "X.509" )
                    .generateCertificate( new ByteArrayInputStream( rep ) );
            certB.verify( caPublicKey );
            this.output.write( "OK".getBytes() );
            byte[] sessionKey = this.read();
            this.output.write( "OK".getBytes() );
            byte[] signed = this.read();
            if ( AsymetricKeyManager.verifySig( certB, sessionKey, signed ) ) {
                session = AsymetricKeyManager.decipher( myKey, sessionKey );
            }
        } catch ( Exception e ) {
            e.printStackTrace();
            finishedOK = false;
        }
    }

    public byte[] getSessionKey() {
        return this.session;
    }

    public DataInputStream getInputStream() {
        return this.input;
    }

    public DataOutputStream getOutputStream() {
        return this.output;
    }

    public Socket getSocketBack() {
        return this.s;
    }

    public String getUserName() {
        return this.certB.getSerialNumber().toString();
    }
}
