package clients;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class Client {

    public static final String KEYSTORE_FILE  = "keystores/user_keystore.ks";
    public static final String PASSWORD       = "passwd";
    public static final String CA_ADDRESS     = "localhost";
    public static final int    CA_PORT        = 7001;
    public static final String ALIAS_CA       = "ca_certificate";
    public static final String ALIAS_CERT     = "alice_certificat";
    public static final String ALIAS_KEY      = "alice_private";
    public static final String KEYPASS        = "monpassP1";

    public static String       SERVER_ADDRESS = "127.0.0.1";
    public static int          SERVER_PORT    = 7000;

    private X509Certificate    myCert;
    private PrivateKey         myKey;
    private X509Certificate    caCert;
    private KeyStore           myKeyStore;
    Socket                     socket;
    byte[]                     sessionkey;
    DataOutputStream           output;
    DataInputStream            input;
    CopyManager                copy;
    Authentification           auth;
    public static String       source;
    public static String       destination;
    public static int          action;

    public Client() {
        try {
            if ( !hasKeyStore() ) {
                createCertificat();
            } else {
                chargeCertificat();
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void chargeCertificat() throws Exception {
        myKeyStore = KeyStore.getInstance( KeyStore.getDefaultType() );
        myKeyStore.load( new FileInputStream( KEYSTORE_FILE ), PASSWORD.toCharArray() );

        if ( myKeyStore.containsAlias( ALIAS_CERT ) )
            myCert = (X509Certificate) myKeyStore.getCertificate( ALIAS_CERT );
        if ( myKeyStore.containsAlias( ALIAS_KEY ) )
            myKey = (PrivateKey) myKeyStore.getKey( ALIAS_KEY, KEYPASS.toCharArray() );
        if ( myKeyStore.containsAlias( ALIAS_CA ) )
            caCert = (X509Certificate) myKeyStore.getCertificate( ALIAS_CA );
    }

    public void createCertificat() throws Exception {
        myKeyStore = KeyStore.getInstance( KeyStore.getDefaultType() );
        myKeyStore.load( null, PASSWORD.toCharArray() );
        CertificateRequest certRequest = new CertificateRequest( CA_ADDRESS, CA_PORT, KEYSTORE_FILE, PASSWORD );
        certRequest.connect();
        certRequest.run();
        certRequest.storeCertAndKey( myKeyStore, ALIAS_CERT, ALIAS_KEY, KEYPASS, ALIAS_CA );
        myKeyStore.load( new FileInputStream( KEYSTORE_FILE ), PASSWORD.toCharArray() );
        if ( myKeyStore.containsAlias( ALIAS_CERT ) )
            myCert = (X509Certificate) myKeyStore.getCertificate( ALIAS_CERT );
        if ( myKeyStore.containsAlias( ALIAS_KEY ) )
            myKey = (PrivateKey) myKeyStore.getKey( ALIAS_KEY, KEYPASS.toCharArray() );
        if ( myKeyStore.containsAlias( ALIAS_CA ) )
            caCert = (X509Certificate) myKeyStore.getCertificate( ALIAS_CA );
    }

    public void run() {
        try {
            auth = new Authentification( SERVER_ADDRESS, SERVER_PORT, socket, this.myCert,
                    this.myKey, caCert.getPublicKey() );

            auth.connect();
            auth.run();
            String user = auth.certB.getIssuerDN().getName();
            System.out.println( user );
            sessionkey = auth.getSessionKey();
            this.socket = auth.getSocketBack();
            copy = new CopyManager( SERVER_ADDRESS, SERVER_PORT, this.socket, sessionkey, source, destination, action );
            copy.setUser( user );
            copy.bind();
            copy.run();
            copy.close();
            if ( copy.finishedWell() )
                System.out.println( "Done." );
            else {
                System.out.println( copy.geterrorMsg() );
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public X509Certificate getMyCert() {
        return myCert;
    }

    public void setMyCert( X509Certificate myCert ) {
        this.myCert = myCert;
    }

    public boolean hasKeyStore() {
        File f = new File( KEYSTORE_FILE );
        return f.exists();
    }

    public static void main( String[] args ) {
        action = args[0].equals( "-c" ) ? 0 : 1;
        source = args[1];
        destination = args[2];
        if ( args.length > 3 ) {
            SERVER_PORT = Integer.parseInt( args[3] );
            SERVER_ADDRESS = args[4];
        }
        Client client = new Client();
        client.run();
    }

}
