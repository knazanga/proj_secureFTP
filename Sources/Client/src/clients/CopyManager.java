package clients;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

public class CopyManager extends Connection {
    public static final int COPY = 0;   // Copy from server
    public static final int SEND = 1;   // Send to server
    String                  source;
    String                  destination;
    String                  user;
    int                     action;
    byte[]                  key;

    public CopyManager( String ip, Integer port, Socket s, byte[] key, String source, String dest, int act ) {
        super( ip, port );
        this.s = s;
        this.key = key;
        this.source = source;
        this.action = act;
        this.destination = dest;
    }

    public void bind() {
        try {
            this.output = new DataOutputStream( this.s.getOutputStream() );
            this.input = new DataInputStream( new DataInputStream( this.s.getInputStream() ) );
        } catch ( IOException e ) {
            this.finishedOK = false;
        }
    }

    @Override
    public void run() {
        try {
            if ( this.action == COPY ) {
                sendHeader( action, source );
                this.read();
                byte[] received = this.read();
                received = SymmetricKeyManager.decipher( key, received );
                FileOutputStream fos = new FileOutputStream( destination );
                fos.write( received );
                fos.close();
                System.out.println( "Fichier bien recu!" );
            } else {
                sendHeader( action, destination );
                input.read();
                FileInputStream fis = new FileInputStream( source );
                byte[] data = new byte[fis.available()];
                fis.read( data );
                fis.close();
                byte[] toSend = SymmetricKeyManager.cipher( key, data );
                this.output.write( toSend );
                System.out.println( "Fichier envoyé avec succès" );
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void sendHeader( int act, String path ) {
        System.out.println( "Envoi entete!" );
        try {
            byte[] header = ( "" + act + " " + path ).getBytes();
            header = SymmetricKeyManager.cipher( key, header );
            output.write( header );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void setUser( String user ) {
        this.user = user;
    }

}
