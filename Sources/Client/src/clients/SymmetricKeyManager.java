package clients;

import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class SymmetricKeyManager {
    public static final String CIPHER_ALGORITHM = "AES";

    public static byte[] cipher( byte[] key, byte[] data ) throws Exception {
        key = Arrays.copyOf( key, 16 );
        SecretKeySpec skeySpec = new SecretKeySpec( key, CIPHER_ALGORITHM );
        Cipher cipher = Cipher.getInstance( CIPHER_ALGORITHM );
        cipher.init( Cipher.ENCRYPT_MODE, skeySpec );
        return cipher.doFinal( data );
    }

    public static byte[] decipher( byte[] key, byte[] data ) throws Exception {
        key = Arrays.copyOf( key, 16 );
        SecretKeySpec skeySpec = new SecretKeySpec( key, CIPHER_ALGORITHM );
        Cipher cipher = Cipher.getInstance( CIPHER_ALGORITHM );
        cipher.init( Cipher.DECRYPT_MODE, skeySpec );
        return cipher.doFinal( data );
    }

}
