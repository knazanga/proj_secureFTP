package serverFTP;

public class MySecurityManager extends SecurityManager {
    String userName;

    public MySecurityManager( String user ) {
        super();
        System.out.println( userName );
        this.userName = user;
    }

    @Override
    public void checkRead( String file ) {
        if ( !file.startsWith( userName ) )
            throw new SecurityException( "Vous n'avez pas le droit de lire dans ce repertoire" );
    }

    @Override
    public void checkWrite( String file ) {
        // System.out.println( file );
        if ( !file.startsWith( userName ) )
            throw new SecurityException( "Vous n'avez pas le droit d'écrire dans ce repertoire" );
    }
}
