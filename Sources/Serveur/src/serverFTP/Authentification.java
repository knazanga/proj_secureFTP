package serverFTP;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Authentification extends Connection {
    PublicKey       caPublicKey;
    boolean         isServer;
    X509Certificate myCert;
    PrivateKey      myKey;
    byte[]          session;
    X509Certificate certB;

    public Authentification( String ip, Integer port, Socket s, X509Certificate cA,
            PrivateKey kA, PublicKey pk ) {
        super( ip, port );
        this.s = s;
        this.myCert = cA;
        this.myKey = kA;
        this.caPublicKey = pk;
    }

    public void bind() {
        try {
            this.output = new DataOutputStream( this.s.getOutputStream() );
            this.input = new DataInputStream( new DataInputStream( this.s.getInputStream() ) );
        } catch ( IOException e ) {
            this.finishedOK = false;
        }
    }

    public void run() {
        try {
            byte[] rep = this.read();
            this.certB = (X509Certificate) CertificateFactory.getInstance( "X.509" )
                    .generateCertificate( new ByteArrayInputStream( rep ) );
            certB.verify( caPublicKey );
            this.output.write( this.myCert.getEncoded() );
            this.read();
            byte[] sessionKey = generateSessionKey();
            this.session = sessionKey;
            sessionKey = AsymetricKeyManager.cipher( certB, sessionKey );
            this.output.write( sessionKey );
            this.read();
            byte[] sessionKeySigned = AsymetricKeyManager.sign( myKey, sessionKey );
            this.output.write( sessionKeySigned );
        } catch ( Exception e ) {
            e.printStackTrace();
            finishedOK = false;
        }
    }

    public byte[] generateSessionKey() throws Exception {
        Security.addProvider( new BouncyCastleProvider() );
        KeyPairGenerator kpg = KeyPairGenerator.getInstance( "DH" );
        KeyPair kp = kpg.generateKeyPair();
        DHPrivateKey privateKey = (DHPrivateKey) kp.getPrivate();
        DHPublicKey publicKey = (DHPublicKey) kp.getPublic();
        KeyAgreement keyAgreement = KeyAgreement.getInstance( "DH" );
        keyAgreement.init( privateKey );
        keyAgreement.doPhase( publicKey, true );
        SecretKey key = keyAgreement.generateSecret( "AES" );
        return key.getEncoded();
    }

    public byte[] getSessionKey() {
        return this.session;
    }

    public DataInputStream getInputStream() {
        return this.input;
    }

    public DataOutputStream getOutputStream() {
        return this.output;
    }

    public Socket getSocketBack() {
        return this.s;
    }

    public String getUserName() {
        String name = this.certB.getSubjectDN().getName();
        return name.substring( 3, name.length() );
    }
}
