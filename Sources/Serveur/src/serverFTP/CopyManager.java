package serverFTP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

public class CopyManager extends Connection {
    public static final int COPY = 0;   // copy from server
    public static final int SEND = 1;   // send to server
    String                  source;
    String                  destination;
    String                  user;
    int                     action;
    boolean                 type;
    byte[]                  key;

    public CopyManager( String ip, Integer port, Socket s, byte[] key, String source, String dest, int act,
            boolean type ) {
        super( ip, port );
        this.s = s;
        this.key = key;
        this.source = source;
        this.action = act;
        this.destination = dest;
        this.type = type;
    }

    public void bind() {
        try {
            this.output = new DataOutputStream( this.s.getOutputStream() );
            this.input = new DataInputStream( new DataInputStream( this.s.getInputStream() ) );
        } catch ( IOException e ) {
            this.finishedOK = false;
        }
    }

    @Override
    public void run() {
        try {
            receiveHeader();
            mkDir( user );
            // MySecurityManager sm = new MySecurityManager( source );
            // System.setSecurityManager( sm );

            if ( source.indexOf( "/" ) == -1 ) {
                source = user + "/" + source;
                System.out.println( source );
            }
            if ( this.action == SEND ) {
                // sm.checkPermission( new FilePermission( source, "read" ) );
                byte[] received = this.read();
                received = SymmetricKeyManager.decipher( key, received );
                FileOutputStream fos = new FileOutputStream( source );
                fos.write( received );
                fos.close();
                System.out.println( "Fichier bien recu!" );
            } else {
                // sm.checkPermission( new FilePermission( source, "write" ) );
                FileInputStream fis = new FileInputStream( source );
                byte[] data = new byte[fis.available()];
                fis.read( data );
                fis.close();
                byte[] toSend = SymmetricKeyManager.cipher( key, data );
                this.output.write( toSend );
                System.out.println( "Fichier envoyé avec succès" );
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void sendHeader( int act, String path ) {
        try {
            byte[] header = ( "" + act + " " + path ).getBytes();
            header = SymmetricKeyManager.cipher( key, header );
            output.write( header );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void receiveHeader() {
        try {
            byte[] header = this.read();
            String entete = new String( SymmetricKeyManager.decipher( key, header ) );
            this.action = Integer.parseInt( entete.split( " " )[0] );
            this.source = entete.split( " " )[1];
            output.write( "OK".getBytes() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void setUser( String user ) {
        this.user = user;
    }

    public void mkDir( String dir ) {
        File f = new File( dir );
        if ( !f.exists() )
            f.mkdirs();
    }

}
