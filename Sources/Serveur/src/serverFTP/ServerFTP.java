package serverFTP;

import java.io.File;
import java.io.FileInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class ServerFTP {

    public static final String KEYSTORE_FILE = "keystores/server_keystore.ks";
    public static final String PASSWORD      = "passwd";

    public static final String ALIAS_CA      = "ca_certificate";
    public static final String CA_ADDRESS    = "localhost";
    public static final int    CA_PORT       = 7001;

    public static final int    SERVER_PORT   = 7000;

    public static final String ALIA_CERT     = "alice_certificat";
    public static final String ALIA_KEY      = "alice_private";
    public static final String KEYPASS       = "servpass";

    private X509Certificate    myCert;
    private PrivateKey         myKey;
    private X509Certificate    caCert;
    private KeyStore           myKeyStore;
    ServerSocket               server_sock;

    public ServerFTP() {
        try {
            if ( !hasKeyStore() ) {
                createCertificat();
            } else {
                chargeCertificat();
            }
            this.server_sock = new ServerSocket( SERVER_PORT );
            System.out.println( "Wait for a connection..." );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void chargeCertificat() throws Exception {

        myKeyStore = KeyStore.getInstance( KeyStore.getDefaultType() );
        myKeyStore.load( new FileInputStream( KEYSTORE_FILE ), PASSWORD.toCharArray() );

        if ( myKeyStore.containsAlias( ALIA_CERT ) )
            myCert = (X509Certificate) myKeyStore.getCertificate( ALIA_CERT );
        if ( myKeyStore.containsAlias( ALIA_KEY ) )
            myKey = (PrivateKey) myKeyStore.getKey( ALIA_KEY, KEYPASS.toCharArray() );
        if ( myKeyStore.containsAlias( ALIAS_CA ) )
            caCert = (X509Certificate) myKeyStore.getCertificate( ALIAS_CA );
    }

    public void createCertificat() throws Exception {

        myKeyStore = KeyStore.getInstance( KeyStore.getDefaultType() );
        myKeyStore.load( null, PASSWORD.toCharArray() );
        CertificateRequest certRequest = new CertificateRequest( CA_ADDRESS, CA_PORT, KEYSTORE_FILE, PASSWORD );
        certRequest.connect();
        certRequest.run();
        certRequest.storeCertAndKey( myKeyStore, ALIA_CERT, ALIA_KEY, KEYPASS, ALIAS_CA );

        myKeyStore.load( new FileInputStream( KEYSTORE_FILE ), PASSWORD.toCharArray() );
        if ( myKeyStore.containsAlias( ALIA_CERT ) )
            myCert = (X509Certificate) myKeyStore.getCertificate( ALIA_CERT );
        if ( myKeyStore.containsAlias( ALIA_KEY ) )
            myKey = (PrivateKey) myKeyStore.getKey( ALIA_KEY, KEYPASS.toCharArray() );
        if ( myKeyStore.containsAlias( ALIAS_CA ) )
            caCert = (X509Certificate) myKeyStore.getCertificate( ALIAS_CA );
    }

    public void run() {
        while ( true ) {
            try {
                if ( this.server_sock == null ) {
                    System.out.println( "socket null" );
                    break;
                }
                Socket socket = this.server_sock.accept();
                System.out.println( "Client accepted: " + socket.getLocalSocketAddress().toString() );
                Thread manager = new Thread(
                        new ScpClientManager( socket, this.myKey, this.myCert, caCert.getPublicKey() ) );
                manager.start();
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }
    }

    public boolean hasKeyStore() {
        File f = new File( KEYSTORE_FILE );
        return f.exists();
    }

    public static void main( String[] args ) {
        ServerFTP server = new ServerFTP();
        server.run();
    }
}
