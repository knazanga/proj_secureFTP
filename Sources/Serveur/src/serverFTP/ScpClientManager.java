package serverFTP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;

public class ScpClientManager implements Runnable {
    private X509Certificate myCert;
    private PrivateKey      myKey;
    private PublicKey       caPublicKey;
    Socket                  client;
    byte[]                  sessionkey;
    DataOutputStream        out;
    DataInputStream         in;
    Authentification        auth;

    public ScpClientManager( Socket sc, PrivateKey key, X509Certificate cert, PublicKey pk ) {
        this.client = sc;
        this.myKey = key;
        this.myCert = cert;
        this.caPublicKey = pk;
    }

    @Override
    public void run() {
        try {
            auth = new Authentification( "localhost", ServerFTP.SERVER_PORT, client, this.myCert,
                    this.myKey, caPublicKey );
            auth.bind();
            auth.run();
            this.in = auth.getInputStream();
            this.out = auth.getOutputStream();
            sessionkey = auth.getSessionKey();
            client = auth.getSocketBack();
            String user = auth.getUserName();
            System.out.println( user );
            CopyManager cm = new CopyManager( "localhost", ServerFTP.SERVER_PORT, client, sessionkey, null,
                    null, 0, true );
            cm.setUser( user );
            cm.bind();
            cm.run();
            cm.close();
            client.close();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

}
